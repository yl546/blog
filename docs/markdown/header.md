---
author: 一只羊
title: 标题语法
Date: 2024-03-26 16:38:25
LastEditTime: 2024-03-26 16:56:40
---

## 标题语法

要创建标题，需要在单词或者短语前面加（#）;#的数量代表标题的级别。

| Markdown 语法     | HTML 语法                | 预览效果            |
| ----------------- | ------------------------ | ------------------- |
| # Heading level 1 | <h1>heading level 1</h1> | ### Heading level 1 |

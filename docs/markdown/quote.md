---
author: 一只羊
title: 引用语法
Date: 2024-03-26 17:17:53
LastEditTime: 2024-03-26 17:24:57
---

## 引用语法

创建块引用，在段落前加一个>符号。

> good good study, day day up

### **多个段落的块引用**

每一行都加 >

> 你引用的啥子
>
> 引用的 good good study, day day up

### **嵌套使用**

听说你还可以套娃

> 套娃就套娃
>
> > 怎么了

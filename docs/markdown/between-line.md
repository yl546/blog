---
author: 一只羊
title: 分隔线
Date: 2024-03-26 17:54:29
LastEditTime: 2024-03-26 17:59:51
---

## 分隔线

使用三个或者多个星号、破折号或下划线,并且不能包含其他内容

向我看齐

---

向他看齐

---

向后看齐

---

### **最佳实践**

三个破折号`---`

---
author: 一只羊
title: 图片语法
Date: 2024-03-26 18:09:08
LastEditTime: 2024-03-26 18:17:12
---

## 图片语法

添加图片之前一定要使用`!`

```
  适用语法![图片alt](图片链接 "图片title")
```

渲染效果如下:

![demo.jpg](/assets/demo.jpg "魔法花园")

---
author: 一只羊
title: 代码语法
Date: 2024-03-26 17:39:37
LastEditTime: 2024-03-26 17:49:02
---

## 代码语法

要将单词或短语表示为代码，请将其包裹在反引号 (`) 中

```html
<html>
  <head></head>
</html>
```

```js
function demo() {
  console.log("Hello World");
}
```

```json
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
```

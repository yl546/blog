/*
 * @author: 一只羊
 * @title: 一只羊
 * @Date: 2024-03-26 11:54:08
 * @LastEditTime: 2024-03-28 16:51:16
 */
module.exports = {
  title: "一只🐏",
  description: "一只羊的小小博客",
  base: "/blog/",
  locales: {
    "/": {
      lang: "zh-CN",
    },
  },
  theme: "reco",
  themeConfig: {
    nav: [
      { text: "首页", link: "/" },
      { text: "Markdown 语法", link: "/markdown/" },
      { text: "JavaScript", link: "/javascript/" },
      {
        text: "一只🐏的仓库",
        items: [
          { text: "Github", link: "https://github.com/yl546" },
          {
            text: "掘金",
            link: "https://juejin.cn/user/712139234359182/posts",
          },
        ],
      },
    ],
    sidebar: {
      "/markdown": [
        {
          title: "基础语法",
          collapsable: false,
          path: "/markdown",
          children: [
            { title: "标题语法", path: "/markdown/header" },
            { title: "段落语法", path: "/markdown/paragraph" },
            { title: "换行语法", path: "/markdown/line" },
            { title: "强调语法", path: "/markdown/stress" },
            { title: "引用语法", path: "/markdown/quote" },
            { title: "列表语法", path: "/markdown/list" },
            { title: "代码语法", path: "/markdown/code" },
            { title: "分隔线语法", path: "/markdown/between-line" },
            { title: "链接语法", path: "/markdown/link" },
            { title: "图片语法", path: "/markdown/image" },
          ],
        },
        {
          title: "拓展语法",
          collapsable: false,
          path: "/markdown-extended",
          children: [
            { title: "表格语法", path: "/markdown-extended/table" },
            { title: "围栏代码块", path: "/markdown-extended/blocks" },
            { title: "定义列表", path: "/markdown-extended/list" },
            { title: "删除线", path: "/markdown-extended/strikethrough" },
            { title: "任务列表", path: "/markdown-extended/task-list" },
          ],
        },
      ],
      "/javascript": [
        {
          title: "JavaScript",
          path: "/javascript",
          collapsable: false,
          children: [
            { title: "解构", path: "/javascript/解构" },
            { title: "迭代器与生成器", path: "/javascript/迭代器与生成器" },
          ],
        },
      ],
    },
  },
};

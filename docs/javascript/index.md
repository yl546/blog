---
author: 一只羊
title: JavaScript
Date: 2024-03-28 14:31:45
LastEditTime: 2024-03-28 15:43:40
---

## JavaScript 是什么

Javascript 是一门编程语言。

一门能够将用户行为通过不同形式直观展现出来的编程语言，一门能赋予静态页面生命的语言。

前端我们首先学习的就是 HTML,这相当于人体的骨架；然后我们学习 CSS,它可以让我们对页面进行美化，赋予了网页血肉；
JavaScript 就像是人的神经，给予网页更多的活力。

## JavaScript 能做什么

- web 开发
- 移动应用开发
- 服务器和 API(node.js)
- 桌面应用（Electron）  
  ...

不应该限制 JavaScript 的功能，他的功能依赖于底层基础的强大。

## 定义列表

缩进的 list

```
First Term
: This is the definition of the first term.

Second Term
: This is one definition of the second term.
: This is another definition of the second term.
```

```html
<dl>
  <dt>First Term</dt>
  <dd>This is the definition of the first term.</dd>
  <dt>Second Term</dt>
  <dd>This is one definition of the second term.</dd>
  <dd>This is another definition of the second term.</dd>
</dl>
```

呈现效果如下：

First Term  
: This is the definition of the first term.

Second Term  
: This is one definition of the second term.  
: This is another definition of the second term.

list 1  
: list 1-1  
: list 1-2

list 2  
: list 2-1

哟西竟然没有效果

#!/usr/bin/env sh

# 确保脚本抛出遇到的错误
set -e

# 生成静态文件
yarn build

# 进入生成的文件夹
cd docs/.vuepress/dist

# 创建流水线配置文件
cat > .gitlab-ci.yml <<EOF
pages:
  stage: deploy
  script:
    - mkdir .public
    - cp -r ./* .public
    - rm -rf public
    - mv .public public
  artifacts:
    paths:
      - public
EOF

git init
git config user.email 1017273765@qq.com
git config user.password Yl9264546
git add -A
git commit -m 'deploy'

# 如果发布到 https://<USERNAME>.github.io/<REPO>
git push -f git@gitlab.com:yl546/blog.git master:gh-pages

cd -